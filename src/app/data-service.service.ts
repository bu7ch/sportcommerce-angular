import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  data: [] = [];

  constructor() { }

  getData(): [] {
    return this.data;
  }

}
