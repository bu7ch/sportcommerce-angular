import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  datas : String[] = []
  constructor() {}

  addMessage(str: String) {
    this.datas.push(str);
  }

  getMessage() {
    return this.datas
  }
}
